import axios from "axios";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { Close, Embed, Label, Box, Image, Flex, Card } from 'theme-ui'
import { VideoAndInfor } from "../component/VideoAndInfor";

export async function getServerSideProps(context) {
  const apiHeader: any = {
    // "#Authorization":"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMjUwMywiY29udGVudEZpbHRlciI6IjE2IiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQzNzc2ODIsImV4cCI6MTYwNDM3ODU4Mn0.8giyQvoMFB_093COB6-yfrUtcCuuJK830v9DgBhrYqEIieQ8iiKPdjLKmfb-RBhPYLH1xCojmlZ3kquT00RBhw",
    "deviceid": 'wap_lx4230g34f420ns43laskl342sl76',
    "devicetype": 'Android',
    lang: 'vi',
    osapptype: 'WAP',
    osappversion: '8.1',
    sessionid: 'l50fdl23fl4lg0asl4309f20234',
    zoneid: '1',
    Accept: '*/*',
    "Accept-Encoding": "gzip, deflate, br",
    "Content-Type": "application/json",
    "Connection": 'keep-alive',
    "User-Agent": "PostmanRuntime/7.30.1"
  }

  let dataSe: any

  dataSe = await axios.get(`http://api.tv360.vn/public/v1/composite/get-live-detail?id=${context.params.id}`, { headers: apiHeader })
    .then(function (response) {
      return response.data
    }).catch(function (error) {
      console.error(error);
    });

  return {
    props: { data: dataSe ?? {} }, // will be passed to the page component as props
  }
}

export default function item(data: any) {  
  return (
    <>
      <Head>
        <title>oke</title>
      </Head>
      <Flex sx={{ margin: 32 }}>
        <Box sx={{ width:'70%', height:'60%' }}>
          <Embed sx={{ width: '90%', height: '70%', alignSelf: 'center' }} src={data.data.data.detail.link}>
          </Embed>
          <Label>
            {data.data.data.detail.seoDescription}
          </Label>
          <Label>
            {data.data.data.detail.seoTitle}
          </Label>
        </Box>
        <Box sx={{width:'26%',height:'30%'}}>

          <VideoAndInfor></VideoAndInfor>
        </Box>
      </Flex>
    </>
  )
}