export const checkToken = (token: string) => {
  const CryptoJS = require("crypto-js")
  const arr = String(token).split('.')
  const parsedWordArray = CryptoJS.enc.Base64.parse(arr[1]);
  const parsedStr = parsedWordArray.toString(CryptoJS.enc.Utf8);
  const position = String(parsedStr).search('exp')
  const timeEndRequest = Number(String(parsedStr).substring(position + 5, position + 15))
  if(arr.length === 3 && timeEndRequest > Math.round(Date.now() / 1000)){
    return true
  }
  else{
    return false
  }
}