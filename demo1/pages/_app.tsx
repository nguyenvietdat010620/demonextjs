import '../styles/globals.css'
import React, { createContext, useEffect, useState } from 'react';
import { appWithTranslation } from 'next-i18next'
import { SessionProvider } from "next-auth/react"
import { AppContext } from './helper/AppContext';
import { Inter } from 'next/font/google'

export const themeContext = createContext({})
const inter = Inter({
  weight: '300',
  subsets: ['latin']
})

const MyApp = ({ Component, pageProps: { session, ...pageProps } }) => {

  return (
    // <CookiesProvider>
      <SessionProvider session={session}>
        <AppContext>
          <main className={inter.className}>
            <Component {...pageProps} />
          </main>
        </AppContext>
      </SessionProvider>
    // </CookiesProvider>

  )
}

export default appWithTranslation(MyApp)
