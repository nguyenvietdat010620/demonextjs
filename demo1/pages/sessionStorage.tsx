import { useEffect, useState } from "react"
import { Input, Label, NavLink, Image, Container, Box, Field, Flex, Button } from 'theme-ui'
import { getDataSessionStorage } from "./helper/sessionStorageService"

export default function sessionStorage() {
  const [data, setdata] = useState('')
  const getDataSession = () => {
    setdata(getDataSessionStorage('datsession'))
  }
  useEffect(() => {
    getDataSession()
  })

  return (
    <Box>
      <Label>{data}</Label>
    </Box>
  )
}