import { useEffect, useState } from "react"
import { toast, ToastContainer } from "react-toastify"
import { Input, Label, NavLink, Image, Container, Box, Field, Flex, Button } from 'theme-ui'
import { ToastTv } from "./component/Toast"
// import { getcookie } from "./helper/cockiteService"
import { getDataStorage, setDataStorage } from "./helper/storageFunc"
import 'react-toastify/dist/ReactToastify.css';
import useTrans from "./hooks/useTrans"
import { useRouter } from "next/router"
export default function cookie() {
  const [cokiii, setcokiii] = useState('')
  const [a, seta] = useState(0)
  const [b, setb] = useState(0)

  useEffect(() => {
    getCooki()
  })

  const trans = useTrans()
  const router = useRouter()

  const changeLang = (lang) => {
    router.push('/', '/', { locale: lang })
  }

  const getCooki = () => {
    // setcokiii(getcookie('token'))
  }

  const onClick = () => {
    ToastTv.showSuccess('dat oke')
  }

  const chagea = () => {
    seta(a + 1)
  }

  const chageb = () => {
    setb(b + 1)
  }

  const chageab = () => {
    seta(a + 1)
    setb(b + 1)
  }

  useEffect(() => {
    function celebrityName(firstName) {
      var nameIntro = "This celebrity is ";
      // Đây là hàm bên trong mà có thể truy cập đến biến của hàm bên ngoài, truy cập được tham số của hàm ngoài.
      function lastName(theLastName) {
        return nameIntro + firstName + " " + theLastName;
      }
      return lastName;
    }

    var mjName = celebrityName("Michael"); //celebrityName (bên ngoài) đã trả về.

    // Closure (lastName) được goi ở đây sau khi hàm ngoài đã trả về.
    // Closure vẫn có thể truy cập được biến và tham số của hàm bên ngoài.
    // mjName("Jackson"); // This celebrity is Michael Jackson.
  }, [])

  return (
    <Box>
      <Label>{cokiii}</Label>
      <Button onClick={onClick}>action toast</Button>
      <ToastContainer />
      <Button onClick={() => changeLang('vn')}>
        VN
      </Button>
      <Button onClick={() => changeLang('en')}>
        EN
      </Button>
      <Button onClick={chagea}>
        Change a
      </Button>
      <Button onClick={chageb}>
        Change b
      </Button>
      <Button onClick={chageab}>
        Change a + b
      </Button>
    </Box>
  )
}