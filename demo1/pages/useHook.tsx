import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { Input, Label, NavLink, Image, Container, Box, Field, Flex, Button } from 'theme-ui'
import { getDataStorage, setDataStorage } from "./helper/storageFunc"
import useHookAny from "./hooks/useHookAny"
import useTrans from "./hooks/useTrans"
import rsa from 'js-crypto-rsa'
import useArrToBase64 from "./hooks/useArrToBase64"

export default function useHook() {
  const [pub, setpub] = useState(null)
  const [pri, setpri] = useState(null)
  const [url, seturl] = useState('')
 const token = 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.pazba9Pj009HgANP4pTCQAHpXNU7pVbjIGff_plktSzsa9rXTGzFngaawzXGEO6Q0Hx5dtGi-dMDlIadV81o3Q'
//  const token = '123abc'
  const route = useRouter()
  useEffect(() => {
    rsa.generateKey(2048).then((key) => {
      // now you get the JWK public and private keys
      const publicKey = key.publicKey;
      const privateKey = key.privateKey;
      setpub(publicKey)
      setpri(privateKey)
      console.log('publicKey:', JSON.stringify(publicKey,null,2))
      console.log('privateKey:', JSON.stringify(privateKey,null,2))
    })
  },[])
  
  function stringToArray(bufferString) {
    let uint8Array = new TextEncoder("utf-8").encode(bufferString);
    return uint8Array;
  }
  function arrayToString(bufferValue) {
    return new TextDecoder("utf-8").decode(bufferValue);
  }

  
  
  
  const sign1 = () => {
    rsa.encrypt(
      stringToArray(token),
      pub,
      'SHA-1', // optional, for OAEP. default is 'SHA-256'
    ).then((encrypted) => {
      console.log('123', useArrToBase64(encrypted))
      // now you get an encrypted message in Uint8Array
      return rsa.decrypt(
        encrypted,
        pri,
        'SHA-1', // optional, for OAEP. default is 'SHA-256'
      );
    }).then((decrypted) => {
      console.log('decrypted', decrypted)
      console.log('encrypted', arrayToString(decrypted))
      // now you get the decrypted message
    });

  }

  return (
    <Box>
      <Label>{useHookAny(10206003000, 3)}</Label>
      <Box onClick={()=>{
        sign1()
      }}>asdasd</Box>
    </Box>
  )
}