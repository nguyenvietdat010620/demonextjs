export function setDataStorage(key:string, value:string){
  localStorage.setItem(key, value)
}

export function getDataStorage(key:string){
  const data = localStorage.getItem(key)
  return data
}