import React, { useEffect, useState } from 'react';
import { Box, Flex, Image, StylePropertyValue, Text } from 'theme-ui';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';
import { motion, useMotionValue, useTransform, animate } from "framer-motion";
import { useFlubber, getIndex } from '../component/SVG/use-flubber';
import { playVideo, pause } from '../component/SVG';
import PlayPause from '../component/Animation/PlayPause';
import { Display } from 'next/dist/compiled/@next/font';
import NextNProgress from 'nextjs-progressbar';
import SoundControl from '../component/SoundControl';
import ProgressBar from '../component/Progressbar';

export const VideoJS = (props) => {
  const videoRef = React.useRef(null);
  const playerRef = React.useRef(null);
  const playerInitTimeRef = React.useRef(0);
  const { options, onReady, nextVideo, preVideo } = props;
  const [isHover, setisHover] = useState(false)
  const [isHoverSound, serisHoverSound] = useState(false)
  const [disPlay, setdisPlay] = useState<StylePropertyValue<Display>>(false)


  const handlePlayerReady = (player) => {
    playerRef.current = player;
    // You can handle player events here, for example:

    player.on('pause', () => {

    })

    player.on('play', () => {
      console.log('play')
      // setautoPlay(true)
    })

    player.on('waiting', () => {

    });

    player.on('dispose', () => {

    });

    player.on('timeupdate', () => {
    })

    player.on('seeked', () => {
      console.log('seeked')
    })

    player.on('volumechange', () => {
      console.log('volumechange')
    })

    player.on('ratechange', () => {
      console.log('ratechange')
    })

    player.on('ended', () => {
      nextVideo()
    })

    let qualityLevels = player.qualityLevels();
    qualityLevels.on('addqualitylevel', function (event) {
      let qualityLevel = event.qualityLevel;

      if (qualityLevel.height >= 720) {
        qualityLevel.enabled = true;
      } else {
        qualityLevel.enabled = false;
      }
    });
    qualityLevels.on('change', function () {
    });
  };

  useEffect(() => {
    playerInitTimeRef.current = Date.now();
    // Make sure Video.js player is only initialized once
    if (!playerRef.current) {
      // The Video.js player needs to be _inside_ the component el for React 18 Strict Mode. 
      const videoElement = document.createElement("video-js");
      videoElement.classList.add('vjs-big-play-centered');

      videoRef.current.appendChild(videoElement);

      const player = playerRef.current = videojs(videoElement, options, () => {
        player.currentTime()
        handlePlayerReady(player);
      });

      // You could update an existing player in the `else` block here
      // on prop change, for example:
    } else {
      const player = playerRef.current;
      player.autoplay(options.autoplay);
      player.src(options.sources);
      player.currentTime(options.currentTime)
      player.volume(options.volume)
      player.playbackRate(options.playbackRate)

    }
  }, [options, videoRef]);


  useEffect(() => {
    const player = playerRef.current;
    return () => {
      if (player && !player.isDisposed()) {
        player.dispose();
        playerRef.current = null;
      }
    };
  }, [playerRef]);


  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      const player = playerRef.current;
      if (e.code === 'Space') {
        e.preventDefault();
        e.stopPropagation();
      }
      if (e.code === 'ArrowLeft') {
        player.currentTime(player.currentTime() - 5)
      }
      if (e.code === 'ArrowRight') {
        player.currentTime(player.currentTime() + 5)
      }
      if (e.key === "i") {
        player.volume(player.volume() + 0.1)
      }
      if (e.key === "d") {
        player.volume(player.volume() - 0.1)
      }
      if (e.key === "n") {
        nextVideo()
      }
      if (e.key === "p") {
        preVideo()
      }
    };
    document.addEventListener('keydown', handler);
    return () => {
      document.removeEventListener('keydown', handler);
    };
  }, [])

  const colors = [
    "#fff",
    "#fff",
  ];
  const [pathIndex, setPathIndex] = useState(0);
  const progress = useMotionValue(pathIndex);
  const paths = [playVideo, pause];
  const fill = useTransform(progress, paths.map(getIndex), colors);
  const path = useFlubber(progress, paths);
  const changeState = () => {
    const player = playerRef.current;
    const animation = animate(progress, pathIndex, {
      duration: 0.3,
      ease: "easeInOut",
      onComplete: () => {
        if (pathIndex === paths.length - 1) {
          setPathIndex(0)
          player.play()
        }
        else {
          setPathIndex(pathIndex + 1)
          player.pause()
        }
      }
    })
  }

  const fullScreen = () => {
    const player = playerRef.current;
    if (!player.isFullscreen()) {
      player.requestFullscreen()
    }
  }

  const renderPlayPause = () => {
    return (
      <Box onClick={changeState} sx={{ justifyContent: 'center', height: 40, width: 50, flexDirection: 'row' }}>
        <svg style={{ height: 40, width: 50 }}>
          <g style={{ height: 40, width: 50, justifyContent: 'center' }} transform="translate(10 10) scale(1 1)">
            <motion.path fill={fill} d={path} />
          </g>
        </svg>
      </Box>
    )
  }

  const renderFullScreen = () => {
    return (
      <Image onClick={() => {
        fullScreen()
      }} sx={{ height: 20, width: 20, marginRight: 2 }} src='/fullScreen.png'></Image>
    )
  }

  const renderPreviousNext = () => {
    return (
      <Flex sx={{ flexDirection: 'row', marginBottom: 0.5 }}>
        <Image onClick={() => {
          preVideo()
        }} sx={{ height: 35, width: 30 }} src='/previous.png'></Image>
        <Image onClick={() => {
          nextVideo()
        }} sx={{ height: 35, width: 30 }} src='/next.png'></Image>
      </Flex>
    )
  }

  const renerVolume = () => {
    return (
      <Flex sx={{ flexDirection: 'row', alignItems:'center' }}>
        <Image 
        onMouseOver={() => {
          console.log('onMouseOver')
          serisHoverSound(true)
        }}
        onMouseLeave={() => {
          console.log('onMouseLeave')
          serisHoverSound(false)
        }}
        sx={{ height: 30, width: 30 }} src='/volume.png'></Image>
        {/* <motion.div
          style={{
            height: 40, width: '100%',
            justifyContent: 'center', position: 'relative', bottom: 45,
            alignItems: 'center', flexDirection: 'row',
            // backgroundColor: 'transparent',
          }}
          transition={{
            duration: 0.3,
            delay: 0.1,
            // ease: [0, 0.71, 0.2, 1.01]
          }}
          initial={{ opacity: isHoverSound ? 0 : 1 }}
          animate={{ opacity: isHoverSound ? 1 : 0 }}
        >
          <ProgressBar bgcolor={'blue'} completed={70}></ProgressBar>
        </motion.div> */}
      </Flex>
    )
  }


  return (
    <Box
      onMouseOver={() => {
        console.log('onMouseOver')
        setisHover(true)
      }}
      onMouseLeave={() => {
        console.log('onMouseLeave')
        setisHover(false)
      }}
    >
      <div data-vjs-player>
        <div ref={videoRef} />
      </div>
      <motion.div
        style={{
          height: 40, width: '100%',
          justifyContent: 'center', position: 'relative', bottom: 45,
          alignItems: 'center', flexDirection: 'row',
          // backgroundColor: 'transparent',
          backgroundColor: 'red'
        }}
        transition={{
          duration: 0.3,
          delay: 0.1,
          // ease: [0, 0.71, 0.2, 1.01]
        }}
        initial={{ opacity: isHover ? 0 : 1 }}
        animate={{ opacity: isHover ? 1 : 0 }}
      >
        {/* <NextNProgress color="#29D" ></NextNProgress> */}
        <Flex sx={{ flexDirection: 'row', justifyContent: 'space-between', height: 40, width: '100%', alignItems: 'center' }}>
          <Flex sx={{ flexDirection: 'row', height: 40, width: '100%', alignItems: 'center' }}>
            {renderPlayPause()}
            {renerVolume()}
            {renderPreviousNext()}
          </Flex>
          {renderFullScreen()}
        </Flex>
      </motion.div>
    </Box>
  );
}

export default VideoJS;