export function setDataSessionStorage(key:string, value:string){
  sessionStorage.setItem(key, value)
}

export function getDataSessionStorage(key:string){
  return sessionStorage.getItem(key)
}