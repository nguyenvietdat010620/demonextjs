const CryptoJS = require("crypto-js")
export function Encrypt(word, key = 'dat162000') {
  const encJson = CryptoJS.AES.encrypt(JSON.stringify(word), key).toString()
  const encData = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encJson))
  return encData
}

export function Decrypt(word, key = 'dat162000') {
  const decData = CryptoJS.enc.Base64.parse(word).toString(CryptoJS.enc.Utf8)
  const bytes = CryptoJS.AES.decrypt(decData, key).toString(CryptoJS.enc.Utf8)
  console.log('bytes--', bytes)
  return JSON.parse(bytes)
}