import React, { createContext, useEffect, useState } from 'react';
import { getDataStorage, setDataStorage } from './storageFunc';
export const AppContextTheme = createContext({})

export const AppContext = ({children}) =>{
  const [initColor, setColor] = useState('black')
  
  useEffect(() => {
    setColor(getDataStorage('color') ? getDataStorage('color') : 'black')
  })

  const changeTheme = ()=>{
    setColor(initColor === 'red' ? 'black' : 'red')
    setDataStorage('color',initColor === 'red' ? 'black' : 'red' )
  }
  return <AppContextTheme.Provider value={{theme: initColor, changeTheme: changeTheme}}>
    {children}
  </AppContextTheme.Provider>
}