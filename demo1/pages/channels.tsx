import React, { useEffect, useState } from "react"
import axios from 'axios'
import { useRouter } from "next/router";
import { Input, Label, NavLink, Image, Container, Box } from 'theme-ui'
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';
import Head from "next/head";
import useDrag from "./helper/useDrag";

export default function userData() {
  const [data, setData] = useState([])
  const route = useRouter()
  type scrollVisibilityApiType = React.ContextType<typeof VisibilityContext>;
  const apiHeader: any = {
    deviceid: 'wap_lx4230g34f420ns43laskl342sl76',
    devicetype: 'Android',
    lang: 'vi',
    osapptype: 'WAP',
    osappversion: '8.1',
    sessionid: 'l50fdl23fl4lg0asl4309f20234'
  }

  const getData = () => {
    axios.get(`http://api.tv360.vn/public/v1/live/get-detail-category?id=5&offset=0&limit=10000`, apiHeader)
      .then((res: any) => {
        const data = res.data.data
        setData(res.data.data)
      })
      .catch((err: any) => {
        console.log('data err', err);
      })
  }

  useEffect(() => {
    getData()
  }, [])
  const { dragStart, dragStop, dragMove, dragging } = useDrag();


  const handleDrag =
    ({ scrollContainer }: scrollVisibilityApiType) =>
      (ev: React.MouseEvent) =>
        dragMove(ev, (posDiff) => {
          if (scrollContainer.current) {
            scrollContainer.current.scrollLeft += posDiff;
          }
        });

  return (
    <>
      <Head>
        <title>Danh sach</title>
      </Head>
      {/* <Header /> */}
      <div>
        {
          data.map((item, index) => {
            return (
              <div key={index}>
                <div>
                  {item.description}
                </div>
                <ScrollMenu
                  transitionDuration={500}
                  onMouseMove={handleDrag}
                  onMouseUp={() => dragStop}
                  onMouseDown={() => dragStart}
                >
                  {
                    item.content.map((item: any, index: number) => {
                      return (
                        // <Box sx={{ height: 250, width: 300 }}>
                        <NavLink sx={{
                          borderRadius: 10,
                          backgroundColor: 'red',
                          marginRight: 16,
                          transform: 'translate(-10%, -10%)',
                          transition: 'transform .5s',
                          ':hover': {
                            transform: 'translate(-10%, -10%) scale(1.2) ',
                            border: '10px solid gray',
                          }
                        }}
                          className='11111'
                          key={index} href={`http://localhost:3000/channels/${item.id}`}>
                          <img style={{ height: 220, width: 340, padding: 20 }} src={item.coverImage ? item.coverImage : "/tv360.png"}></img>
                          <div>{item.name}</div>
                        </NavLink>
                        // </Box>
                      )
                    })
                  }
                </ScrollMenu>
              </div>
            )
          })
        }
      </div>
    </>
  )
}