import { Close, Embed, Label,Box } from 'theme-ui'

export const VideoAndInfor = ({
  listChanel
}: {
  listChanel?: any
}) => {

  const listdata= [
    {
      url:"https://www.youtube.com/embed/GNCd_ERZvZM",
      title:"tes 1"
    },
    {
      url:"https://www.youtube.com/embed/GNCd_ERZvZM",
      title:"tes 2"
    },
    {
      url:"https://www.youtube.com/embed/GNCd_ERZvZM",
      title:"tes 3"
    }
  ]
  return (
    <Box>
      {listdata.map((item:any, index:any) => {
        return (
          <Box key={index}>
            <Embed sx={{height:300,width:300}} src={item.url}></Embed>
            <Box>{item.title}</Box>
          </Box>
        )
      })}
    </Box>
  )
}