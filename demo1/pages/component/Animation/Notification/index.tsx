import * as React from "react";
import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { wrap } from "popmotion";
import { Box, Flex, Image } from "theme-ui";
// import { images } from "./image-data";

const variants = {
  enter: (direction: number) => {
    return {
      x: direction > 0 ? 1000 : -1000,
      opacity: 0
    };
  },
  center: {
    zIndex: 1,
    x: 0,
    opacity: 1
  },
  exit: (direction: number) => {
    return {
      zIndex: 0,
      x: direction < 0 ? 1000 : -1000,
      opacity: 0
    };
  }
};

/**
 * Experimenting with distilling swipe offset and velocity into a single variable, so the
 * less distance a user has swiped, the more velocity they need to register as a swipe.
 * Should accomodate longer swipes and short flicks without having binary checks on
 * just distance thresholds and velocity > 0.
 */
const swipeConfidenceThreshold = 10000;
const swipePower = (offset: number, velocity: number) => {
  return Math.abs(offset) * velocity;
};


export const Example = () => {
  const [[page, direction], setPage] = useState([0, 0]);
  const [stateHover ,setstateHover] = useState(null)
  const data = [{
    id: 1
  }, {
    id: 2
  }, {
    id: 3
  },
  {
    id: 4
  },
  {
    id: 5
  }, {
    id: 6
  }]

  const Item = ({ind}: {ind?: number}) => (
    <motion.div
      style={{ margin: 5,height:150, width:160 }}
      whileHover={{ scale: 1.5 }}
      animate={{ opacity: 0.9 }}
      exit={{ opacity: 0 }}
      onHoverStart={(a,b)=>{
        // setstateHover()
      }}
    >
      <Image src={'https://upload.wikimedia.org/wikipedia/commons/1/13/Logo_PTIT_University.png'}></Image>
    </motion.div>
  
)

  return (
    <Box sx={{height:190, alignItems:'center', justifyContent:'center'}}>
      <AnimatePresence initial={false} custom={direction}>
        <Flex sx={{flexDirection:'row', alignItems:'center', justifyContent:'center', width:900}}>
          {data.map((item, index) => (
            <Item ind={index}></Item>
          ))}
        </Flex>
      </AnimatePresence>
    </Box>
  );
};
