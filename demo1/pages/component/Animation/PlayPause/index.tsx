import { Box, Button, Flex } from "theme-ui";
import { motion, useMotionValue, useTransform, animate } from "framer-motion";
import { playVideo, pause } from "../../SVG";
import { useFlubber, getIndex } from "../../SVG/use-flubber";
import { useState } from "react";

const PlayPause = ({ isPlaying }: { isPlaying?: boolean }) => {
  const paths = [playVideo, pause];
  const colors = [
    "#00cc88",
    "#0099ff",
  ];
  const [pathIndex, setPathIndex] = useState(0);
  const progress = useMotionValue(pathIndex);
  const fill = useTransform(progress, paths.map(getIndex), colors);
  const path = useFlubber(progress, paths);

  const changeState = () => {
    const animation = animate(progress, pathIndex, {
      duration: 0.3,
      ease: "easeInOut",
      onComplete: () => {
        if (pathIndex === paths.length - 1 ) {
          setPathIndex(0)
        }
        else if (pathIndex != paths.length - 1 ) {
          setPathIndex(pathIndex + 1)
        }
      }
    })
  }

  return (
    <Box onClick={changeState} 
    sx={{ backgroundColor: 'blue', justifyContent: 'center', height: 40, width: 50}}>
      <svg style={{ height:40, width:50}}>
        <g style={{height:40, width:50}} transform="translate(10 10) scale(1 1)">
          <motion.path fill={fill} d={path} />
        </g>
      </svg>
    </Box>
  )
}

export default PlayPause