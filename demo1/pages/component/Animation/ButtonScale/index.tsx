import { motion } from "framer-motion"

export const ButtonScale = ({ children }: { children: any }) => (
  <motion.div
    whileHover={{ scale: 1.3 }}
    onHoverStart={e => { }}
    onHoverEnd={e => { }}>
    {children}
  </motion.div>
)