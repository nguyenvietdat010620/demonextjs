import { motion } from "framer-motion"

export const HoverScale = ({ children }: { children: any }) => (
  <motion.div
    whileHover={{ scale: 1.3, border: '2px solod white' }}
    whileTap={{ scale: 0.8 }}>
    {children}
  </motion.div>
)