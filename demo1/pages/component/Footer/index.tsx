import { Flex, Box, Image, Text } from "theme-ui";
import { useContext } from "react";
import { AppContextTheme } from "../../helper/AppContext";

export default function Footer() {
  const a:any = useContext(AppContextTheme)
  const theme = a.theme

  return (
    <footer>
      <Flex sx={{ height: 244, backgroundColor: theme, justifyContent: 'space-between',paddingLeft:32, paddingRight:32, paddingTop:16, paddingBottom:16,
       '@media screen and (max-width: 720px)': {
              flexDirection:'column',
              height:500
            }, }}>
        <Flex sx={{ width: '15%', alignItems:'center', justifyContent:'center','@media screen and (max-width: 720px)': {
              width:'100%',
            } }}>
          <Image sx={{ height: 56, width: 117 }} src='/tv360.png'></Image>
        </Flex>
        <Flex sx={{ width: '40%', flexDirection: 'column', justifyContent: 'space-between', '@media screen and (max-width: 720px)': {
              width:'100%',
            } }}>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Cơ quan chủ quản: Tổng Công ty Viễn thông Viettel (Viettel Telecom) - Chi nhánh Tập đoàn Công nghiệp - Viễn thông Quân đội.</Text>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Mã số doanh nghiệp: 0100109106-011 do Sở Kế hoạch và Đầu tư Thành phố Hà Nội cấp lần đầu ngày 18/07/2005, sửa đổi lần thứ 15 ngày 18/12/2018.</Text>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Chịu trách nhiệm nội dụng: Ông Cao Anh Sơn</Text>
          <Flex sx={{flexDirection:'row', justifyItems:'center', alignItems:'center'}}>
            <Flex sx={{alignItems:'center'}}>
              <Image sx={{height:24, width:24, marginRight:10}} src="/call.png"></Image>
              <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>1800 8098/198</Text>
            </Flex>
            <Flex sx={{alignItems:'center', marginLeft:16}}>
              <Image sx={{height:24, width:24, marginRight:10}} src="/call.png"></Image>
              <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Hỗ Trợ</Text>
            </Flex>
          </Flex>
        </Flex>
        <Flex sx={{ width: '10%', flexDirection: 'column', justifyContent: 'space-between', '@media screen and (max-width: 720px)': {
              width:'100%',
              flexDirection:'row'
            } }}>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Gói dịch vụ</Text>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Quảng cáo</Text>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Chính sách</Text>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Liên hệ</Text>
          <Text></Text>
        </Flex>
        <Flex sx={{ width: '25%', flexDirection:'column', justifyContent: 'space-between','@media screen and (max-width: 720px)': {
              width:'100%'
            } }}>
          <Text sx={{color:'white', fontSize:15, fontWeight:'500'}}>Tải App để xem hơn 150 Kênh truyền hình và hàng vạn Phim HD bom tấn. Miễn cước truy cập 3G/4G tốc độ cao.</Text>
          <Flex sx={{justifyContent:'space-between'}} >
            <Image sx={{height:47, width:142}} src='/androidStore.png'></Image>
            <Image sx={{height:47, width:142}} src='/appleStore.png'></Image>
          </Flex>
          <Image sx={{height:76, width:200}} src='/tradeMark.png'></Image>
        </Flex>
      </Flex>
    </footer>
  )
}