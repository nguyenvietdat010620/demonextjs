import { Box, Button, Flex, Image, Label, Link } from 'theme-ui'
import { useEffect, useState } from 'react'
import { AiOutlineBars } from "react-icons/ai"
import { useContext } from "react";
import axiosConfig from '../../interceptor/configAxios'
import { ToastTv } from '../Toast'
import useTrans from '../../hooks/useTrans'
import { AppContextTheme } from '../../helper/AppContext'
import { useSession, signOut } from "next-auth/react"
import { useRouter } from 'next/router'
import { deleteCookie, getCookie } from 'cookies-next'
import { ButtonScale } from '../Animation/ButtonScale';

export default function Header() {
  const t = useTrans()
  const { data } = useSession()
  const route = useRouter()

  // const [name, setname] = useState('')
  // useEffect(() => {
  //   setname(getCookie('name'))
  // }, [])
  const header: any = {
    // 'Content-Type': 'text/plain',
    'User-Agent': 'Mozilla/5.0 (Web0S; Linux/SmartTV) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36 WebAppManager',
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
    'devicetype': 'TIZEN',
    'lang': 'vi',
    'osapptype': 'osapptype',
    'osappversion': '1.3.0',
    'sessionid': '635f4d7a-45f0-4c71-9943-865db7c4252a',
    // 'zoneid': '1',
    // '#Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMTgzNCwiY29udGVudEZpbHRlciI6IjEzIiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQwNTcwMTUsImV4cCI6MTYwNDA1NzMxNX0.-ZJNkJyTtwkmHlyU-lIro5jWjWbzEazEkQZMlwcnobNEL5vjXp6qRstj5wv4ci_BORcM5izLyKv4z5LoRm0MHg'
  }

  const listTitle = [
    {
      title: t.header.home_page,
      link: '/home',
    },
    {
      title: t.header.activity,
      link: '/test'
    },
    {
      title: t.header.contact,
      link: '/paint'
    },
    {
      title: t.header.fanpage
    }
  ]

  const listBtn = [
    {
      title: 'Trang chủ',
      icon: '/notification_icon.png',
    },
    {
      title: 'Hoạt động',
      icon: '/information_chat.png'
    },
    {
      title: 'Liên hệ',
      icon: data ? data.user.image : '/profile.png'
    },
  ]

  const profile = [
    {
      title:
        data
          ? data.user.name
          : getCookie('name')
            ? getCookie('name')
            : 'Cá nhân'
      // action: ''
    },
    {
      title: 'Đổi mật khẩu'
    },
    {
      title: 'Đăng xuất'
    }
  ]

  const notification = [
    {
      title: 'Bạn vừa đăng kí gói K+ thành công, dịch vụ sẽ được áp dụng từ ngày 16/8/2022'
    },
    {
      title: 'Gói K+ của bạn sắp hết hạn vui lòng đăng kí để có trải nghiệm tốt nhất'
    },
    {
      title: 'Chúc bạn một ngày tốt lành'
    }
  ]

  const [isPopoverOpen, setIsPopoverOpen] = useState(true)
  const [indexBtn, setindexBtn] = useState(1)
  const a: any = useContext(AppContextTheme)
  const theme = a.theme

  const clearCookie = () => {
    deleteCookie('name')
    deleteCookie('token')
    deleteCookie('refresh')
  }

  const signOutApi = () => {
    const data = {
      "refreshToken": getCookie('refresh'),
      "deviceId": 'p5782fw4-38a6-4466-v753-24b55661365'
    }
    axiosConfig.post('api/v1/user/logout', data, { headers: header })
      .then((res: any) => {
        route.push('/login')
      })
  }

  const logOut = () => {
    clearCookie()
    if (data) { signOut() }
    else {
      signOutApi()
    }
  }
  return (
    <header>
      <Flex sx={{ height: 80, backgroundColor: theme, }}>
        <Flex sx={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image sx={{ height: 56, width: 117 }} src='/tv360.png'></Image>
        </Flex>
        <Flex sx={{ flex: 2, justifyContent: 'center', alignItems: 'center',
        '@media screen and (max-width: 712px)': {
          display:'none'
        },
      }}>
          {
            listTitle.map((item: any, index: number) => {
              return (
                <ButtonScale>
                  <Link href={item.link} key={index} sx={{ marginRight: 20, color: 'white' }}>
                    {item.title}
                  </Link>
                </ButtonScale>
              )
            })
          }
        </Flex>
        <Flex sx={{ justifyContent: 'space-around', alignItems: 'center', width:150, marginRight:100 }}>
          {
            listBtn.map((item: any, index: number) => {
              return (
                <Box key={index} onClick={() => {
                  setindexBtn(index)
                  ToastTv.showSuccess('Dat dep trai')
                  setIsPopoverOpen(!isPopoverOpen)
                }} sx={{ backgroundColor: theme }}>
                  <Image sx={{ borderRadius: 10, width: 20, height: 20 }} src={item.icon}>
                  </Image>
                </Box>
              )
            })
          }
        </Flex>
      </Flex>
      {isPopoverOpen && indexBtn === 2 &&
        <Flex
          sx={{
            position: 'absolute', height: 120, width: 160,
            flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center',
            backgroundColor: 'red', top: 60, right: ((1 / (indexBtn + 1)) * 90), zIndex: 999
          }}
        >
          {
            profile.map((item, index) => {
              return (
                <Button sx={{ backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center', display: 'flex', flex: 1, borderBottomWidth: 1 }}
                  onClick={logOut}
                >
                  <Label sx={{ display: 'flex', flex: 1, borderBottomWidth: 1, borderBottomColor: 'black' }}>
                    {item.title}
                  </Label>
                </Button>
              )
            })
          }
        </Flex>
      }
      {isPopoverOpen && indexBtn === 0 &&
        <Box
          sx={{ position: 'absolute', height: 200, width: 300, backgroundColor: 'red', top: 60, right: ((1 / (indexBtn + 1)) * 90), zIndex: 999 }}
        >
          {
            notification.map((item, index) => {
              return (
                <Flex key={index} sx={{ alignItems: 'center', px: 10, marginTop: 10 }}>
                  <AiOutlineBars color='white'></AiOutlineBars>
                  <Label sx={{ marginLeft: 10, color: 'white' }}>
                    {item.title}
                  </Label>
                </Flex>
              )
            })
          }
        </Box>
      }
    </header>
  )
}

