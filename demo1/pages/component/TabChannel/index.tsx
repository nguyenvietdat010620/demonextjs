import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { Close, Embed, Label, Box, Image, Flex, Card, NavLink, Button, Text } from 'theme-ui'
import { themeContext } from '../../_app';
import { useContext, useEffect, useState } from 'react';
import axiosConfig from '../../interceptor/configAxios';
import { apisubDomain } from '../../api/apis';
import _ from 'lodash'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/effect-fade';
export default function TabChannel({ listPlaylist, index, changeIndex }: { listPlaylist?: any, index?: number, changeIndex?:any }) {
  const theme = useContext(themeContext)
  const [listData, setlistData] = useState([])
  const header: any = {
    'Connection': 'keep-alive',
    'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
    'devicetype': 'Android',
    'lang': 'vi',
    'osapptype': 'WAP',
    'osappversion': '0.1.0',
    'sessionid': '635f4d7a-45f0-4c71-9943-865db7c4252a',
    'zoneid': '1',
  }
  const getData = async () => {
    await axiosConfig.get(apisubDomain.home_banner_category_get, { headers: header })
      .then((res: any) => {
        const data = res.data.data
        const index = _.findIndex(data, (e: any) => {
          return e.type === "BANNER"
        }, 0)
        const arr_filter = _.filter(data, function (o) {
          return o.content && o.type != "BANNER"
        })
        setlistData(arr_filter)
      })
      .catch((err: any) => {
        console.log(err);
      })
  }
  useEffect(() => {
    getData()
  }, [])

  const changeChannel = (id:any)=>{
    
  }

  return (
    <Box sx={{ marginLeft: 32 }}>
      <Tabs>
        <TabList style={{ backgroundColor: 'black' }}>
          <Tab style={{ backgroundColor: 'black', color: 'white', borderBottomColor: 'red' }}>Các tập</Tab>
          <Tab style={{ backgroundColor: 'black', color: 'white' }}>Tương tự</Tab>
        </TabList>

        <TabPanel>
          <Text sx={{ fontSize: 16, color: 'white' }} >Any content 1</Text>
          {/* <Flex sx={{ flexDirection: 'row' }}> */}
          <SwiperSlide style={{}}>
            {listPlaylist.map((item: any, ind: number) => {
              return (
                <Button onClick={()=>{changeIndex(ind)}} sx={{backgroundColor:'transparent'}}>
                  <Image key={ind} sx={{
                    height: 110, width: 80,
                    border: index === ind ? '10px solid gray' : '1px solid gray'
                  }} src={'https://upload.wikimedia.org/wikipedia/commons/1/13/Logo_PTIT_University.png'}>
                  </Image>
                </Button>
              )
            })}
          </SwiperSlide>
          {/* </Flex> */}
        </TabPanel>
        <TabPanel>
          <Box sx={{ backgroundColor: theme, marginLeft: 32, marginRight: 32 }}>
            {
              listData.map((item) => {
                return (
                  <Box>
                    <Label sx={{ color: 'white' }}>{item.name}</Label>
                    <Swiper
                      spaceBetween={10}
                      slidesPerView={6}
                      navigation
                      pagination={{ clickable: true }}
                      scrollbar={{ draggable: true }}
                      onSwiper={(swiper) => {}}
                      onSlideChange={() => {}}
                    >
                      {item.content.map((item: any, index: number) => {
                        return (
                          <SwiperSlide style={{}} key={index}>
                            <Image sx={{

                            }} src={item.coverImage}>
                            </Image>
                          </SwiperSlide>
                        )
                      })}
                    </Swiper>
                  </Box>
                )
              })
            }
          </Box>
        </TabPanel>
      </Tabs>
    </Box>
  )
}