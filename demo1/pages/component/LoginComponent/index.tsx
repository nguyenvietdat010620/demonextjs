import React, { useEffect, useState, useRef } from "react"
import axios from 'axios'
import { useRouter } from "next/router";
import { Input, Label, Flex, Box, Text, Button, Checkbox, Image } from 'theme-ui'
import _ from 'lodash'
import 'swiper/css';
import 'swiper/css/effect-fade';
import { useContext } from "react";
import { log } from "console";
import { useSession, signIn, signOut } from "next-auth/react"
import { AppContextTheme } from "../../helper/AppContext";
import { apisubDomain } from "../../api/apis";
import { setCookie } from "cookies-next";

export default function LoginComponent({
  isModal, closeModal }: { isModal?: boolean, closeModal?: any }) {
  const [account, setaccount] = useState('')
  const [password, setpassword] = useState('')
  const route = useRouter()
  const a:any = useContext(AppContextTheme)
  const theme = a.theme

  const header: any = {
    // 'Content-Type': 'text/plain',
    'User-Agent': 'Mozilla/5.0 (Web0S; Linux/SmartTV) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36 WebAppManager',
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
    'devicetype': 'TIZEN',
    'lang': 'vi',
    'osapptype': 'osapptype',
    'osappversion': '1.3.0',
    'sessionid': '635f4d7a-45f0-4c71-9943-865db7c4252a',
    // 'zoneid': '1',
    // '#Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMTgzNCwiY29udGVudEZpbHRlciI6IjEzIiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQwNTcwMTUsImV4cCI6MTYwNDA1NzMxNX0.-ZJNkJyTtwkmHlyU-lIro5jWjWbzEazEkQZMlwcnobNEL5vjXp6qRstj5wv4ci_BORcM5izLyKv4z5LoRm0MHg'
  }

  const actionLogin = async () => {
    const data = {
      "msisdn": account,
      "grantType": "PASS",
      "password": password,
      "deviceInfo": {
        "deviceId": "p5782fw4-38a6-4466-v753-24b55661365",
        "osType": "WEBOS",
        "osVersion": "1.3.0"
      }
    }
    await axios.post(`http://api.tv360.vn${apisubDomain.login_post}`, data, { headers: header })
      .then((res: any) => {
        setCookie('token', res.data.data.accessToken)
        setCookie('name', res.data.data.name)
        setCookie('refresh', res.data.data.refreshToken)
        
        if (isModal === false) {
          route.push('/home')
        }
        else{
          closeModal()
        }
      })
      .catch((err: any) => {
        console.log('err', err);

      })
  }

  const loginFaceBook = () => {
    signIn()
  }

  return (
    <Flex sx={{ justifyContent: 'center', alignItems: 'center', backgroundColor: theme }}>
      <Box sx={{ height: 400, width: 350, backgroundColor: '#141415', justifyContent: 'center', borderRadius: 8 }}>
        <Flex sx={{ justifyContent: 'center', alignItems: 'center', height: 60 }}>
          <Text sx={{ fontSize: 20, fontWeight: '700', color: 'white' }}>Đăng nhập</Text>
        </Flex>
        <Box sx={{ margin: 16 }}>
          <Label sx={{ color: 'white', fontSize: 15, fontWeight: '600', marginBottom: 2 }}>Số điện thoại</Label>
          <Input sx={{ borderWidth: 1, borderColor: 'white', color: 'white' }}
            value={account}
            onChange={e => {
              setaccount(e.target.value)
            }}
          />
          <Label sx={{ color: 'white', fontSize: 15, fontWeight: '600', marginTop: 16, marginBottom: 2 }}>Mật khẩu</Label>
          <Input sx={{ borderWidth: 1, borderColor: 'white', color: 'white' }}
            value={password}
            type="password"
            onChange={e => {
              setpassword(e.target.value)
            }}
          />
        </Box>
        <Flex sx={{ marginLeft: 16, alignItems: 'center' }}>
          <Checkbox
            defaultChecked={true}
          ></Checkbox>
          <Text sx={{ color: 'white', fontSize: 15, fontWeight: '600' }}>Ghi nhớ đăng nhập</Text>
        </Flex>
        <Box sx={{ justifyContent: 'center', alignItems: 'center', margin: 16 }}>
          <Button onClick={actionLogin} sx={{ width: '100%', height: 46, backgroundColor: 'red' }}>Đăng nhập</Button>
        </Box>
        <Box sx={{ justifyContent: 'center', alignItems: 'center', margin: 16 }}>
          <Button onClick={loginFaceBook} sx={{ width: '100%', height: 46, backgroundColor: 'red' }}>
            <Text>Đăng nhập bên thứ 3</Text>
          </Button>
        </Box>
      </Box>
    </Flex>
  )

}