import axios from "axios";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Box, Flex, NavLink, Text } from 'theme-ui'
import videojs from "video.js";
import VideoJS from "../helper/VideoJS";
import Header from "../component/HeaderV2";
import { useContext } from "react";
import Footer from "../component/Footer";
import 'react-tabs/style/react-tabs.css';
import TabChannel from "../component/TabChannel";
import { AppContextTheme } from "../helper/AppContext";
import Modal from 'react-modal';
import LoginComponent from "../component/LoginComponent";
import { useSession } from "next-auth/react";
import { getCookie, setCookie } from "cookies-next";
import { checkToken } from "../faker";
import useStringToArr from "../hooks/useStringToArr";
import rsa from 'js-crypto-rsa'
import { pri } from "../Config/const";

export async function getServerSideProps(context) {
  const apiHeader: any = {
    // "#Authorization":"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMjUwMywiY29udGVudEZpbHRlciI6IjE2IiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQzNzc2ODIsImV4cCI6MTYwNDM3ODU4Mn0.8giyQvoMFB_093COB6-yfrUtcCuuJK830v9DgBhrYqEIieQ8iiKPdjLKmfb-RBhPYLH1xCojmlZ3kquT00RBhw",
    "deviceid": 'wap_lx4230g34f420ns43laskl342sl76',
    "devicetype": 'Android',
    lang: 'vi',
    osapptype: 'WAP',
    osappversion: '8.1',
    sessionid: 'l50fdl23fl4lg0asl4309f20234',
    zoneid: '1',
    Accept: '*/*',
    "Accept-Encoding": "gzip, deflate, br",
    "Content-Type": "application/json",
    "Connection": 'keep-alive',
    "User-Agent": "PostmanRuntime/7.30.1"
  }
  const apiHeaderStream: any = {
    // "#Authorization": `Bearer  ${token}`,
    "deviceid": 'wap_lx4230g34f420ns43laskl342sl76',
    "devicetype": 'Chrome',
    "devicename": 'Chrome',
    lang: 'vi',
    osapptype: 'WAP',
    osappversion: '8.1',
    sessionid: 'l50fdl23fl4lg0asl4309f20234',
    // zoneid: '1',
    Accept: '*/*',
    "Accept-Encoding": "gzip, deflate, br",
    "Content-Type": "application/json",
    "Connection": 'keep-alive',
    "User-Agent": "PostmanRuntime/7.30.1",
    "screensize": "360x740",
    "s": "PTt8lWgZiEnQTFbnKem2CO+LX9bCpTZhtKA5h/ecQvvKQhFsEremtdurJ2sdvLE5TjarHiehMYAKY6mdJawcgnUnjg720YEcy4HzOuZTQbumeyN40O9/dNLGGXQrXYqHDZ+yZek4zGLx2raP6nzO2yss+djN1spOr5HxcSRbJhsHQcrPwrxjlNAt5qZarMi3FA2Fpd9uO5ba32YqIMXy9u8OpXTCVv2VFO1HT4bknEaF3ubMgf8KGoBW0OgJkK4QDYtiosk+hPaGR1t2yTjg6AdobA6pFk6I/NmtV1TGWcbTq1K7z8DhTJ3epB5A1R+aqCKxpsT+jVDRWJv94DeDDA=="
  }
  let dataSe: any

  // function stringToArray(bufferString) {
  //   let uint8Array = new TextEncoder("utf-8").encode(bufferString);
  //   return uint8Array;
  // }
  function arrayToString(bufferValue) {
    return new TextDecoder("utf-8").decode(bufferValue);
  }

  dataSe = await axios.get(`http://api.tv360.vn/public/v1/composite/film/get-detail?childId=952909&id=${context.params.id}`, { headers: apiHeader })
    .then((res: any) => {
      // ${res.data.data.detail.id}
      axios.get(`https://api-v2.tv360.vn/public/v1/composite/get-link?id=${res.data.data.detail.id}&type=film&t=1668931437`, { headers: apiHeaderStream })
        .then((res: any) => {

        })
        .catch((err: any) => {
          console.log('err', err);
        })
      return res.data
    }).catch(function (error) {
      console.error(error);
    });

  const toke1 = context.query.token
  if (toke1) {
    const stringreFresh = toke1.replaceAll(' ', '+')
    const arrToken = useStringToArr(stringreFresh)
    rsa.decrypt(
      arrToken,
      pri,
      'SHA-1', // optional, for OAEP. default is 'SHA-256'
    )
    .then((decry)=>{
      console.log('decry', arrayToString(decry))
      setCookie('token', arrayToString(decry),{res: context.res, req: context.req, maxAge:60 * 6 * 24})
      setCookie('name', 'arrToken',{res: context.res, req: context.req, maxAge:60 * 6 * 24})
    })
  }

  // if (toke1) {
  //   const CryptoJS = require("crypto-js")
  //   const stringreFresh = toke1.replaceAll(' ', '+')
  //   const bytes  = CryptoJS.AES.decrypt(stringreFresh, 'dat162000');
  //   const originalText = bytes.toString(CryptoJS.enc.Utf8);
  //   if(checkToken(originalText)){
  //     setCookie('token', originalText,{res: context.res, req: context.req, maxAge:60 * 6 * 24})
  //     console.log('oke')
  //   }
  //   else{
  //     console.log('false')
  //   }
  // }

  return {
    props: {
      dataServerSide: dataSe ?? {},
    }, // will be passed to the page component as props
  }
}

export default function item(dataServerSide: any, token: string) {

  const listPlaylist = [
    'https://vod-zlr2.tv360.vn/video1/2022/05/31/14/93d9fa7d/93d9fa7d-58ed-47e7-879a-0eaae285c2a6_1_.m3u8',
    'https://kd.hd-bophim.com/20230313/33668_0d5c2f8e/index.m3u8',
    'https://live-par-1-abr-cdn.livepush.io/live_abr_cdn/emaIqCGoZw-6/index.m3u8',
    'https://vod-zlr2.tv360.vn/video1/2021/04/12/15/97f97f16/97f97f16-0308-41f6-abca-e2dfbccacccc_1_.m3u8',
    'http://live-ali2.tv360.vn/manifest/VTV5_HD_50fps/playlist_VTV5-HD-50fps-720P-167M.m3u8',
    'https://hd.hdbophim.com/20230329/33561_e4934648/index.m3u8',
    'https://1080.hdphimonline.com/20220315/2087_9b2fb836/index.m3u8'
  ]
  const playerRef = React.useRef(null);
  const a: any = useContext(AppContextTheme)
  const theme = a.theme
  const { data } = useSession()
  const route = useRouter()
  const [open, setisOpen] = useState(false)
  const [currentTime, setcurrentTime] = useState(0)
  const [autoPlay, setautoPlay] = useState(false)
  const [index, setindex] = useState(0)
  let indexPlaylist = 0
  let curren = 0
  const videoJsOptions = {
    autoplay: autoPlay,
    controls: false,
    responsive: true,
    fluid: true,
    currentTime: currentTime,
    playbackRates: [0.5, 0.75, 1, 1.5, 2],
    volume: 0.6,
    userActions: { hotkeys: true },
    // bigPlayButton: false,
    controlBar: {
      children: [
        // 'playToggle',
        // 'progressControl',
        // 'volumePanel',
        // 'qualitySelector',
        // 'fullscreenToggle',
      ],
    },
    sources: [{
      src: listPlaylist[index],
      type: "application/x-mpegURL"
    },
    ]
  };

  const changeIndex = (id: number) => {
    setindex(id)
  }

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  const closeModal = () => {
    setisOpen(false)
    route.push('/home')
  }

  const loginSuccess = () => {
    setisOpen(false)
  }

  const nextVideo = () => {
    console.log('nextVideo')
    indexPlaylist = indexPlaylist + 1
    // if (listPlaylist.length <= indexPlaylist) {
    setindex(indexPlaylist)
    // }
  }

  const preVideo = () => {
    indexPlaylist = indexPlaylist - 1
    // if (indexPlaylist >= 0) {
    setindex(indexPlaylist)
    // }
  }

  // useEffect(() => {
  //   const handler = (e: KeyboardEvent) => {
  //     if (e.code === 'Space') {
  //       e.preventDefault();
  //       e.stopPropagation();
  //     }
  //     if (e.code === 'ArrowLeft') {
  //       setcurrentTime(curren - 5)
  //     }
  //     if (e.code === 'ArrowRight') {
  //       setcurrentTime(curren + 5)
  //     }
  //   };
  //   document.addEventListener('keydown', handler);
  //   return () => {
  //     document.removeEventListener('keydown', handler);
  //   };
  // }, [])


  const bodyDetail = () => {
    return (
      <Flex sx={{ flexDirection: 'column', }}>
        <Flex sx={{
          flexDirection: 'row', justifyContent: 'space-between', marginRight: 32, marginLeft: 32, height: '570px',
          '@media screen and (max-width: 712px)': {
            flexDirection: 'column'
          },
        }}>
          <Box sx={{
            border: '2 solid white',
            height: '800px', width: '750px',
            '@media screen and (max-width: 932px)': {
              height: '500px',
              width: '600px'
            },
            '@media screen and (max-width: 712px)': {
              height: '350px',
              width: '450px'
            },
          }}>
            <VideoJS options={videoJsOptions} nextVideo={nextVideo} preVideo={preVideo}></VideoJS>
          </Box>
          <Box sx={{
            width: '36%',
            '@media screen and (max-width: 712px)': {
              width: '100%'
            },
          }}>
            <Text sx={{ fontSize: 32, color: 'white' }}>{dataServerSide.dataServerSide.data ? dataServerSide.dataServerSide.data.detail.name : "Ten Phim ?"}</Text>
            <Flex sx={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16, marginBottom: 16 }}>
              {
                dataServerSide.dataServerSide.data ?
                  dataServerSide.dataServerSide.data.detail.categories.map((item: any, index: number) => {
                    return (
                      <NavLink key={index} sx={{ fontSize: 16, color: 'white' }}>
                        {item.name}
                      </NavLink>
                    )
                  })
                  : ""
              }
            </Flex>
            <Text sx={{
              fontSize: 16, color: 'white', maxLines: 4, '@media screen and (min-width:712px) and (max-width: 932px)': {
                display: 'none'
              }
            }}>{dataServerSide.dataServerSide.data ? dataServerSide.dataServerSide.data.detail.description : ""}</Text>
            <Flex sx={{ marginTop: 16 }}>
              <Box
                sx={{ width: '20%' }}
              >
                <Text sx={{ fontSize: 12, color: 'white' }}>Diễn viên:</Text>
              </Box>
              <Box
                sx={{ width: '75%' }}
              >
                <Text sx={{ fontSize: 12, color: 'white' }}>
                  {dataServerSide.dataServerSide.data ?
                    dataServerSide.dataServerSide.data.film.infos.reduce((finalString: string, infos: any, index: number) => finalString + infos.name + (index === dataServerSide.dataServerSide.data.film.infos.length - 1 ? "" : ", "), "")
                    : "?"
                  }
                </Text>
              </Box>
            </Flex>
            <Flex sx={{ marginTop: 16 }}>
              <Box
                sx={{ width: '20%' }}
              >
                <Text sx={{ fontSize: 12, color: 'white' }}>Thể loại:</Text>
              </Box>
              <Box
                sx={{ width: '75%' }}
              >
                <Text sx={{ fontSize: 12, color: 'white' }}>
                  {
                    dataServerSide.dataServerSide.data ?
                      dataServerSide.dataServerSide.data.detail.categories.reduce((finalString: string, categories: any, index: number) => finalString + categories.name + (index === dataServerSide.dataServerSide.data.detail.categories.length - 1 ? "" : ", "), "")
                      : "?"
                  }
                </Text>
              </Box>
            </Flex>
          </Box>
        </Flex>
        <TabChannel listPlaylist={listPlaylist} index={index} changeIndex={changeIndex} />
      </Flex>
    )
  }

  return (
    <Box sx={{ backgroundColor: theme }}>
      <Head>
        <title>{dataServerSide.dataServerSide.data ? dataServerSide.dataServerSide.data.detail.name : null}</title>
      </Head>
      <Header />
      {bodyDetail()}
      <Modal
        isOpen={open}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <LoginComponent closeModal={loginSuccess}></LoginComponent>
      </Modal>
      <Footer />
    </Box>
  )
}