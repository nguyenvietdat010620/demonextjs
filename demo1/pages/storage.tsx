import { useEffect, useState } from "react"
import { Input, Label, NavLink, Image, Container, Box, Text, Flex, Button } from 'theme-ui'
import { getDataStorage, setDataStorage } from "./helper/storageFunc"

export default function storage() {
  const [data, setdata] = useState('')
  const [name, setName] = useState('dat dz')
  const getDataLocal = () => {
    let dataOff = localStorage.getItem('names')
    setdata(dataOff)
  }
  useEffect(() => {
    setdata(getDataStorage('dat'))
    getDataLocal()
  })

  const onClickAction = () => {
    setDataStorage("name", name)

  }

  return (
    <Box>
      <Input name={name} id="username" mb={3}></Input>
      <Label>{data}</Label>
      <Button sx={{ backgroundColor: 'red' }} onClick={() => { onClickAction }}>
        <Text sx={{color:'black'}}>Save</Text>
      </Button>
    </Box>
  )
}