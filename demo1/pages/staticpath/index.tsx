import { useEffect, useState } from "react"
import axios from "axios"
import { Flex, Box, Image, Text, Link, Button, Input } from "theme-ui"
import { useRouter } from "next/router"

export async function getStaticProps() {
  const header = {
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept': '*/*',
    'User-Agent': 'PostmanRuntime/7.31.3'
  }

  const listRoom = await axios.get(`https://jsonplaceholder.typicode.com/posts`, { headers: header })
    .then((res: any) => {
      return res.data
    })
  return {
    props: {
      data: listRoom
    },
  }
}

function staticpath({ data }) {
  const [nameRoom, setnameRoom] = useState('')
  const [description, setdescription] = useState('')
  const [code, setcode] = useState('')
  const [action, setaction] = useState('ADD')
  const [id, setid] = useState(0)
  const router = useRouter();
  const header = {
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept': '*/*',
    'User-Agent': 'PostmanRuntime/7.31.3'
  }

  const deleteText = () => {
    setnameRoom('')
    setdescription('')
    setcode('')
  }

  const addRoom = async () => {
    const data = {
      "name": nameRoom,
      "description": description,
      "json_type": "room",
      "code": code
    }
    await axios.post(`http://192.168.66.54:8090/api/room`, data, { headers: header })
      .then((res: any) => {
        router.replace(router.asPath);
        deleteText()
      })
  }

  const deleteRoom = async (item: any) => {
    await axios.delete(`http://192.168.66.54:8090/api/room/${item.id}`, { headers: header })
      .then((res: any) => {
        router.replace(router.asPath);
      })
  }

  const fixRoom = async () => {
    const data = {
      "name": nameRoom,
      "description": description,
      "json_type": "room",
      "code": code
    }
    await axios.patch(`http://192.168.66.54:8090/api/room/${id}`, data, { headers: header })
      .then((res: any) => {
        router.replace(router.asPath);
        deleteText()
        setaction('ADD')
      })
  }

  const setSta = (item: any) => {
    setid(item.id)
    setaction('FIX')
    setnameRoom(item.name)
    setdescription(item.description)
    setcode(item.code)
  }

  return (
    <Flex sx={{ flexDirection: 'row', alignItems: 'center' }}>
      {
        data.map((item: any) => {
          return (
            <Flex sx={{ flexDirection: 'column', marginRight:5 }}>
              <Link href={`/staticpath/${item.id}`}>
                <Box sx={{ height: 250, width: 200, margin: 3 }}>
                  <Image src={'https://upload.wikimedia.org/wikipedia/commons/1/13/Logo_PTIT_University.png'}></Image>
                  <Text>{item.name}</Text>
                </Box>
              </Link>
              <Flex sx={{ flexDirection: 'row', height: 30, alignItems: 'center' }}>
                <Button onClick={() => { deleteRoom(item) }}>Xoa</Button>
                <Button onClick={() => { setSta(item) }}>Sua</Button>
              </Flex>
            </Flex>
          )
        })
      }
      <Flex sx={{ flexDirection: 'column' }}>
        <Flex sx={{ flexDirection: 'column' }}>
          <Text>Tên Phòng</Text>
          <Input sx={{ borderWidth: 1, borderColor: 'black', color: 'black' }}
            value={nameRoom}
            onChange={e => {
              setnameRoom(e.target.value)
            }}
          />
        </Flex>
        <Flex sx={{ flexDirection: 'column' }}>
          <Text>Ghi chu</Text>
          <Input sx={{ borderWidth: 1, borderColor: 'black', color: 'black' }}
            value={description}
            onChange={e => {
              setdescription(e.target.value)
            }}
          />
        </Flex>
        <Flex sx={{ flexDirection: 'column' }}>
          <Text>Code</Text>
          <Input sx={{ borderWidth: 1, borderColor: 'black', color: 'black' }}
            value={code}
            onChange={e => {
              setcode(e.target.value)
            }}
          />
        </Flex>
        <Button onClick={() => {
          if (action === 'ADD') {
            addRoom()
          }
          else {
            fixRoom()
          }
        }} sx={{ height: 50, backgroundColor: 'black' }}>{action === 'ADD' ? 'Thêm phòng' : 'Sửa phòng'}</Button>
      </Flex>
    </Flex>
  )
}
export default staticpath