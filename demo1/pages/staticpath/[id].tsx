import axios from "axios";
import { Box, Button, Flex, Image, Text } from "theme-ui";
import _ from 'lodash'
export async function getStaticPaths() {
  const header = {
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept': '*/*',
    'User-Agent': 'PostmanRuntime/7.31.3'
  }

  const listRoom = await axios.get(`https://jsonplaceholder.typicode.com/posts`, { headers: header })
    .then((res: any) => {
      return res.data
    })
  const paths = listRoom.map((item: any) => {
    return {
      params: { id: `${item.id}` }
    }
  })

  return {
    paths: paths,
    fallback: true
  }
}

export async function getStaticProps(context) {
  const header = {
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept': '*/*',
    'User-Agent': 'PostmanRuntime/7.31.3'
  }
  const data = await axios.get(`https://jsonplaceholder.typicode.com/posts/${context.params.id}`, { headers: header })
    .then((res: any) => {
      const data = res.data
      return data
    })
  return {
    props: {
      dataProperty: data
    },
    revalidate: 10,
  }
  
}

export default function staticPathId(dataProperty) {
  return (
    <Flex sx={{ flexDirection: 'row' }}>
      <Text>{dataProperty.dataProperty.title}</Text>
    </Flex>
  )
}