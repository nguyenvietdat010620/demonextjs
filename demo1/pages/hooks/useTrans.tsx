import { useRouter } from 'next/router'
import en from '../../public/locales/en/en'
import vn from '../../public/locales/vn/vn'


const useTrans = () => {
  const { locale } = useRouter()
  const trans = locale === 'vn' ? vn : en
  return trans
}

export default useTrans