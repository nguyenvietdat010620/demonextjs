// import "./styles.css";
import * as React from "react";
import { useState } from "react";
import { star, heart, hand, plane, lightning, note, playVideo, pause } from "../pages/component/SVG";
import { motion, useMotionValue, useTransform, animate } from "framer-motion";
import { getIndex, useFlubber } from "../pages/component/SVG/use-flubber";
import { Box, Flex, Link } from "theme-ui";



export default function changePath() {
  const paths = [lightning, hand, plane, heart, note, playVideo, pause, lightning];
  const colors = [
    "#00cc88",
    "#0099ff",
    "#8855ff",
    "#ff0055",
    "#ee4444",
    "#ffcc00",
    "#00cc88",
    "#8855ff",
  ];
  const [pathIndex, setPathIndex] = useState(0);
  const progress = useMotionValue(pathIndex);
  const fill = useTransform(progress, paths.map(getIndex), colors);
  const path = useFlubber(progress, paths);

  React.useEffect(() => {
    const animation = animate(progress, pathIndex, {
      duration: 0.8,
      ease: "easeInOut",
      onComplete: () => {
        if (pathIndex === paths.length - 1) {
          progress.set(0);
          setPathIndex(1);
        } else {
          setPathIndex(pathIndex + 1);
        }
      }
    });

    return () => animation.stop();
  }, [pathIndex]);

  return (
    <Flex sx={{flexDirection:'row'}}>
      <svg width="800" height="800">
        <g transform="translate(10 10) scale(17 17)">
          <motion.path fill={fill} d={path} />
        </g>
      </svg>
      <Link href={`/changePaths/${1}`}>asdasdas</Link>
    </Flex>
  );
}
