import React, { useEffect, useState } from "react"
import videojs from "video.js";
import VideoJS from "./helper/VideoJS";
export default function video() {
  const playerRef = React.useRef(null);
  const url = 'https://assets.afcdn.com/video49/20210722/v_645516.m3u8'
  const videoJsOptions = {
    autoplay: true,
    controls: true,
    responsive: true,
    fluid: true,
    sources: [{
      src: url,
      type: "application/x-mpegURL"
    }]
  };

  const handlePlayerReady = (player) => {
    playerRef.current = player;

    // You can handle player events here, for example:
    player.on('waiting', () => {
      videojs.log('player is waiting');
    });

    player.on('dispose', () => {
      videojs.log('player will dispose');
    });
    
  };

  return (
    <>
      <div>Rest of app here</div>
      <VideoJS options={videoJsOptions} onReady={handlePlayerReady}  />
      <div>Rest of app here</div>
    </>
  );
}