import axios from "axios";
import { getCookie, setCookie } from "cookies-next";


let axiosConfig = axios.create({})
const token = getCookie('token')
axiosConfig.defaults.baseURL = 'http://api.tv360.vn'
if(token){
  axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}
axiosConfig.defaults.headers.post['Content-Type'] = 'application/json';
axiosConfig.defaults.headers.common['User-Agent'] = 'PostmanRuntime/7.30.1';
axiosConfig.defaults.headers.common['Accept'] = '*/*';
axiosConfig.defaults.headers.common['Content-Encoding'] = 'gzip, deflate, br';
// axiosConfig.defaults.headers.

// axiosC

const dataRefresh = {
  'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
  'refreshToken': getCookie('refresh')
}

axiosConfig.interceptors.response.use(res => res,
  err => {
    if (err.status === '401') {
      axiosConfig.post('/public/v1/auth/refresh-token', dataRefresh)
        .then((res: any) => {
          setCookie('token', res.data.accessToken)
          setCookie('refresh', res.data.refreshToken)
        })
    }
  })

// axiosConfig.interceptors.request.use()

export default axiosConfig