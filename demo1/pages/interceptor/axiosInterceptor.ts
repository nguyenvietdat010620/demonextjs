import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { getCookie } from "cookies-next";

export default function AxiosInterceptor() {

  const onRequestSuccess = (config: AxiosRequestConfig) => {
    const auth = getCookie('cookie');
    config.timeout = 10000;
    if (auth) {
      config.headers = {
        Authorization: "Bearer " + auth,
        "User-Agent": 'PostmanRuntime/7.30.1',
        Accept: '*/*',
        "Content-Type": 'application/json',
        'Content-Encoding':'gzip, deflate, br',
        'baseURL': 'http://api.tv360.vn',
        
      };
      console.log('onRequestSuccess', config)
      // Xử lý params cho request
      // if (config.params) {
      //   config.paramsSerializer = {
      //     serialize: (params: Record<string, any>) =>
      //       queryString.stringify(params)
      //   }
      // }
    }
    // Các xử lý khác....
    return config;
  };


  const onResponseSuccess = (response: AxiosResponse) => {
    console.log('onResponseSuccess', response)
    return response;
  };

  const onResponseError = (error: AxiosError) => {
    if (
      error.response?.status !== 401 //|| error.config?.url?.includes(authUrl)
    ) {
      console.log('onResponseError 401', error)
      const errMessage = error.response?.data || error?.response || error;
      return Promise.reject(errMessage);
    }
    console.log('onResponseError', error)
    return refreshToken(error); // gọi hàm để refresh token.
  };

  // hàm để refresh token
  const refreshToken = async (error: AxiosError) => {
    const refreshToken = getCookie('refresh');
    if (!refreshToken) {
      // logout();
      return;
    }
    try {
      const dataPost = {
        "refreshToken": refreshToken,
        "deviceId": "p5782fw4-38a6-4466-v753-24b55661365"
      }

      const  data  = await axios.post(`http://api.tv360.vn/public/v1/auth/refresh-token`, dataPost)
      // setLocalStorage({ key: Authenticate.REFRESH_TOKEN_ETC, value: data.refreshToken });
      // setCookie(
      //   Authenticate.AUTH,
      //   JSON.stringify({
      //     username: data.username,
      //     accessToken: data.accessToken,
      //   }),
      //   0.02
      // );
      console.log('AxiosInterceptor' ,data)
      // error.config.headers = {
      //   Authorization: "Bearer " + data.accessToken,
      // }
      return axios(error.config);
    } catch (error) {
      // logout();
      return;
    }
  }

  // axios.interceptors.request.use(onRequestSuccess)
  axios.interceptors.response.use(onResponseSuccess,onResponseError);
}