import React, { useEffect, useState, useRef } from "react"
import axios from 'axios'
import { useRouter } from "next/router";
import { Input, Label, NavLink, Image, Flex, Box, Link } from 'theme-ui'
import Header from "./component/HeaderV2";
import { Swiper, SwiperSlide } from 'swiper/react';
import _ from 'lodash'
import axiosConfig from "./interceptor/configAxios";
import 'swiper/css';
import 'swiper/css/effect-fade';
import { apisubDomain } from "./api/apis";
import Footer from "./component/Footer";
import { useContext } from "react";
import { AppContextTheme } from "./helper/AppContext";
import { SpanAndChange } from "./component/Animation/SpanAndChange";
import { HoverScale } from "./component/Animation/HoverScale";
export async function getServerSideProps(context) {
  const header: any = {
    'Connection': 'keep-alive',
    'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
    'devicetype': 'Android',
    'lang': 'vi',
    'osapptype': 'WAP',
    'osappversion': '0.1.0',
    'sessionid': '635f4d7a-45f0-4c71-9943-865db7c4252a',
    'zoneid': '1',
  }
  const dataBannerAndFilm = await axiosConfig.get(apisubDomain.home_banner_category_get, { headers: header })
    .then((res: any) => {
      const data = res.data.data
      const index = _.findIndex(data, (e: any) => {
        return e.type === "BANNER"
      }, 0)
      const arr_filter = _.filter(data, function (o) {
        return o.content && o.type != "BANNER"
      })
      return [data[index].content, arr_filter]
    })
  return {
    props: { dataBannerAndFilm: dataBannerAndFilm },
  }
}
export default function homeCate(dataBannerAndFilm) {

  const a: any = useContext(AppContextTheme)
  const theme = a.theme
  const swiperScroll = () => {
    return (
      <Box sx={{ backgroundColor: theme, marginLeft: 32, marginRight: 32 }}>
        <Swiper
          // install Swiper modules
          autoplay
          // loop
          spaceBetween={10}
          slidesPerView={3}
          navigation
          pagination={{ clickable: true }}
          scrollbar={{ draggable: true }}
          onSwiper={(swiper) => {}}
          onSlideChange={() => {}}
          onRealIndexChange={(swiper) => {
            
          }}
        >
          {
            dataBannerAndFilm.dataBannerAndFilm[0].map((item: any, index: number) => {
              return (
                <SwiperSlide
                  key={index}
                >
                  <SpanAndChange>
                    <Link href={item.type === 'FILM' ? `/films/${item.id}` : `/home`}>
                      <Image
                        src={item.coverImage}>
                      </Image>
                    </Link>
                  </SpanAndChange>
                </SwiperSlide>
              )
            })
          }
        </Swiper>
      </Box>
    )
  }

  const listVideo = () => {
    return (
      <Box sx={{ backgroundColor: theme, marginLeft: 32, marginRight: 32, padding:16 }}>
        {
          dataBannerAndFilm.dataBannerAndFilm[1].map((item, index) => {
            return (
              <Box key={index}>
                <Label sx={{ color: 'white', fontSize: 14, fontWeight: '600', marginBottom: 2, marginTop: 2 }}>{item.name}</Label>
                <Swiper
                  // install Swiper modules
                  // loop
                  spaceBetween={10}
                  slidesPerView={6}
                  navigation
                  pagination={{ clickable: true }}
                  scrollbar={{ draggable: true }}
                  onSwiper={(swiper) => {}}
                  onSlideChange={() => {}}
                >
                  {item.content.map((item: any, index: number) => {
                    return (
                      <SwiperSlide style={{}} key={index}>
                        <HoverScale>
                          <Image sx={{

                          }} src={item.coverImage}>
                          </Image>
                        </HoverScale>
                      </SwiperSlide>
                    )
                  })}
                </Swiper>
              </Box>
            )
          })
        }
      </Box>
    )
  }

  return (
    <Box sx={{ backgroundColor: theme }}>
      <Header />
      {swiperScroll()}
      {listVideo()}
      <Footer />
    </Box>
  )
}