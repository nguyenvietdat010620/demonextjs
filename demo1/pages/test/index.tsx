import { Box, Flex } from "theme-ui";
import { Example } from "../component/Animation/Notification";
import Header from "../component/HeaderV2";
import PlayPause from "../component/Animation/PlayPause";

export default function test() {
  const data = [{
    id: 1,
    content: '1'
  }, {
    id: 2,
    content: '2'
  },
  {
    id: 3,
    content: '3'
  },
  {
    id: 4,
    content: '4'
  },
  {
    id: 5,
    content: '5'
  },


  ]
  return (
    <Box>
      <Header />
      <Example />
      <PlayPause></PlayPause>
    </Box>
  )
}