import React, { useEffect, useState, useRef } from "react"
import { useRouter } from "next/router";
import Header from "./component/HeaderV2";
import _ from 'lodash'
import 'swiper/css';
import 'swiper/css/effect-fade';
import Footer from "./component/Footer";
import { useSession, signIn, signOut } from "next-auth/react"
import LoginComponent from "./component/LoginComponent";
import { getCookie } from "cookies-next";

export default function login() {

  const route = useRouter()
  const { data } = useSession()

  const checkLogged = () => {
    if (data || getCookie('name')) {
      route.push('/home')
    }
  }

  useEffect(() => {
    checkLogged()
  }, [data])


  return (
    <>
      <Header></Header>
      <LoginComponent isModal={false}></LoginComponent>
      <Footer></Footer>
    </>
  )
}
