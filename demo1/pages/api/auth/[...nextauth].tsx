import NextAuth from "next-auth"
import AppleProvider from "next-auth/providers/apple"
import GoogleProvider from "next-auth/providers/google"
import EmailProvider from "next-auth/providers/email"
import FacebookProvider from "next-auth/providers/facebook";
export const authOptions ={
  // secret: process.env.NODE_ENV,
  secret: "578c7adb0a5f97a2ecb72bc3e0bdb4fa",
  // pages:{
  //   signIn: '/login',
  // },
  providers: [
    FacebookProvider({
      clientId: '1282360502362020',
      clientSecret: '200ed2573fd52b46ee73fef6fd0de641'
    }),
    GoogleProvider({
      clientId: "554872814529-f9uql4l39pmv4l8cc4o320d67actmgqv.apps.googleusercontent.com",
      clientSecret: "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",
      // clientId: process.env.GOOGLE_ID,
      // clientSecret: process.env.GOOGLE_SECRET,
    }),
    // Sign in with passwordless email link
    // EmailProvider({
    //   server: process.env.MAIL_SERVER,
    //   from: "<no-reply@example.com>",
    // }),

  ],
  callbacks: {
    async jwt({ token, account }) {
      // Persist the OAuth access_token to the token right after signin
      if (account) {
        token.accessToken = account.access_token
      }
      return token
    },
    async session({ session, token, user }) {
      // Send properties to the client, like an access_token from a provider.
      session.accessToken = token.accessToken
      return session
    },

    async redirect({ url, baseUrl }) {
      if (url.startsWith("/")) return `/login`
      else if (new URL(url).origin === baseUrl) return url
      return '/login'
    },

    async signIn({ user, account, profile, email, credentials }) {
      const isAllowedToSignIn = true
      if (isAllowedToSignIn) {
        return true
      } else {
        // Return false to display a default error message
        return false
        // Or you can return a URL to redirect to:
        // return '/unauthorized'
      }
    }
    
  }
}

export default NextAuth(authOptions)

