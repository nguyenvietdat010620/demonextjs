import { useContext, useEffect, useState } from "react"
import { Input, Label, NavLink, Image, Container, Box, Field, Flex, Button, Text } from 'theme-ui'
import { AppContextTheme } from "./helper/AppContext"
import Header from "./component/HeaderV2"
import Footer from "./component/Footer"
import { useRouter } from "next/router"

export default function setting() {
  const router = useRouter()
  const a: any = useContext(AppContextTheme)
  const change = a.changeTheme
  const theme = a.theme
  const chooseVN = () => {
    router.push('/setting', 'setting', { locale: 'vn' })
  }

  const chooseEN = () => {
    router.push('/setting', 'setting', { locale: 'en' })
  }

  return (
    <Box >
      <Header></Header>
      <Flex sx={{ backgroundColor: theme, flex: 1, height: 400, flexDirection: 'column', paddingLeft: 102 }}>
        <Box>
          <Text sx={{ color: 'white', fontSize: 13 }}>Change Language</Text>
          <Box>
            <Button onClick={chooseVN}>Tiếng Việt </Button>
            <Button onClick={chooseEN}>Tiếng Anh</Button>
          </Box>
        </Box>
        <Flex sx={{ flexDirection: 'column' }}>
          <Text sx={{ color: 'white', fontSize: 13 }}>Đổi theme</Text>
          <Button sx={{ width: 300 }} onClick={change}>Đổi màu</Button>
        </Flex>
      </Flex>
      <Footer></Footer>
    </Box>
  )
}